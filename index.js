const amqp = require('amqplib/callback_api');
const fs = require('fs');
const async = require("async");
const readline = require("readline");

let channel = null;
const conn = null;
const q = "hello";
let contador = 0;
const start = new Date();
let end = null;
async.series([
    callback => {
        amqp.connect('amqp://127.0.0.1', (err, conn) => {
            conn.createChannel((err, ch) => {
                if(err) {
                    callback(err,"Something is wrong")
                }
                ch.assertQueue(q, {durable: false});
                channel = ch;
                callback(null,"Connected rabbitmq")
            });
        });
    }
],
(err, results) => {
    let rl = readline.createInterface({
        input: fs.createReadStream('my-file.txt')
    });
    
    // event is emitted after each line
    rl.on('line', line => {
        contador++
        channel.publish('', q, new Buffer.from(line))
    });
    
    // end
    rl.on('close', line => {
        console.log(`Total lines : ${contador}`);
        end = new Date() - start
        console.info('Execution time: %dms', end)
    });
});


